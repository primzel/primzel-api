var mongoose = require('./Configurations');
var Schema = mongoose.Schema;
var model = "feed";
var mongoosePaginate = require('mongoose-paginate');
var ObjectId = mongoose.Types.ObjectId;
var User = require('./User');

mongoose.plugin(mongoosePaginate);

var feedSchema = new Schema({
    feedText: String,
    feedMediaUrl: String,
    feedMediaType: {type: String, enum: ["VIDEO", "IMAGE", "NONE"], default: "NONE"},
    createdByName: String,
    createdByEmail: String,
    groupId:String,
    userId:{type:String,required:true},
    like:{type:Number,default:0,min:0},
    deviceToken: String,
    createdDate: {type: Date, default: Date.now}
});

feedSchema.methods.findUser = function (callback) {

    return User.findOne({_id: new ObjectId(this.userId)}, {}, callback);
}

var Feed = mongoose.model(model, feedSchema);

module.exports = Feed;