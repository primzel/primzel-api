var mongoose = require('./Configurations');

var Schema = mongoose.Schema;
var model = "Media";
var mediaSchema = new Schema({
    url: String,
    type: String,
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});


var Media = mongoose.model(model, mediaSchema);

module.exports = Media;