var mongoose = require('./Configurations');
var Group = require('./Group');

var Schema = mongoose.Schema;
var model = "GroupJoinRequest";

var groupJoinRequestSchema = new Schema({
    userId: {type: String, required: true},
    sendAsPush: {type: Boolean, default: true},
    groupId: {type: String, required: true}
});

var GroupJoinRequest = mongoose.model(model, groupJoinRequestSchema);

module.exports = GroupJoinRequest;