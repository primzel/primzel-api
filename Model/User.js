var mongoose = require('./Configurations');
var Group = require('./Group');
var ObjectId = mongoose.Types.ObjectId;

var Schema = mongoose.Schema;
var model = "User";
var userSchema = new Schema({
    contactNumber: {type: String, required: true},
    status: {type: String, required: true},
    displayImage: {type: String, required: true},
    deviceToken: {type: String, required: true},
    groups: [{type: String, trim: true}],
    isDeleted: {type: Boolean, default: false},
    isActive: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

userSchema.methods.findGroups = function (cb) {

    var idList = Array();

    if (this.groups === undefined) {
        this.groups = Array();
    }

    this.groups.forEach(function (e, i) {
        idList.push(new ObjectId(e));
    });

    var query = {$in: idList, isDeleted: false};

    Group.find(query, {}, cb);
}

var User = mongoose.model(model, userSchema);

module.exports = User;