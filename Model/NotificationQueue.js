var mongoose = require('./Configurations');
var Group = require('./Group');

var Schema = mongoose.Schema;
var model = "NotificationQueue";

var notificationSchema = new Schema({
    userId: {type: String, required: true},
    sendAsPush: {type: Boolean, default: true},
    notificationType: {type: String, required: true, default: "NONE", enum: ["GROUP_JOIN_REQUEST", "NEW_FEED_PUSH",]},
    pushDataObject: {type: Object}

});

var NotificationQueue = mongoose.model(model, notificationSchema);

module.exports = NotificationQueue;