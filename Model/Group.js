var mongoose = require("./Configurations");
var mongoosePaginate = require('mongoose-paginate');
var ObjectId = mongoose.Types.ObjectId;

mongoose.plugin(mongoosePaginate);


var Schema = mongoose.Schema;

var genre = ["NONE", "POP", "CLASSIC", "ROCK", "PARTY", "DANCE", "RACE", "GAME"];

var model = "group";

var groupSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, max: 300},
    status: Boolean,
    userId: {type: String, required: true},
    genre: {type: String, default: "NONE", enum: genre},
    isDeleted: Boolean,
    private: Boolean,
    joinModeration: {type: Boolean, default: true},
    createdDate: {type: Date, default: Date.now},
    lastModified: {type: Date, default: Date.now}
});

groupSchema.methods.findGroupById = function (id) {

    var query = {_id: new ObjectId(id), isDeleted: false};

};

var group = mongoose.model(model, groupSchema);

module.exports = group;