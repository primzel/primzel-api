var mongoose = require('./Configurations');
var Schema = mongoose.Schema;
var model = "ApplicationConfiguration";

var applicationConfigurationSchema=Schema({
    name:{type:String,required:true},
    secretKeys:[{phrase:{type:String,required:true},token:{type:String,required:true},method:{type:String,required:true,enum:["GET","POST","PUT","DELETE","OPTION"],default:"GET"} , secret:{type:String,default:"private.key"}}],
    createdDate:{type:Date,Default:Date.now()},
    lastModifiedDate:{type:Date,Default:Date.now()}
});

var ApplicationConfiguration=mongoose.model(model,applicationConfigurationSchema);

module.exports=ApplicationConfiguration;