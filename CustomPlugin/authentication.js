var ApplicationConfiguration = require('../Model/ApplicationConfiguration');
var uuid = require('node-uuid');
var jwt = require('jsonwebtoken');
var ObjectId = require("mongoose").Types.ObjectId;

module.exports = function (onOff) {

    var enabled = onOff === "on" ? true : false;

    return function (req, res, next) {


        if (enabled && req.method !== "GET") {

            var apiKey = req.header("api-key");

            var token = req.header("access-token");

            if (apiKey == undefined || token == undefined) {

                res.send({status: 1002, error: "header {api-key,access-token} is required"})

                return;
            } else {

                ApplicationConfiguration.findOne({_id: new ObjectId(apiKey)}, {}, function (err, result) {

                    if (err || result === undefined || result === null) {
                        res.send({status: 1003, error: "wrong header {api-key}"});
                        return;
                    } else {

                        result.secretKeys.forEach(function (e, i) {
                            if (req.method === e.method) {

                                jwt.verify(token, e.secret, function (err, decoded) {

                                    if (decoded === e.phrase) {

                                        next();
                                    } else {

                                        res.send({status: 1004, error: "wrong header {access-token}"});

                                    }
                                });

                            }
                        });

                    }

                });

            }

        } else {

            next();
        }

    }
}


//var privateP=uuid.v1();
//var privatePU=uuid.v1();
//var privateD=uuid.v1();
//var privateG=uuid.v1();
//var privateO=uuid.v1();
//
//var phraseP=uuid.v1();
//var phrasePU=uuid.v1();
//var phraseD=uuid.v1();
//var phraseG=uuid.v1();
//var phraseO=uuid.v1();
//
//var applicationConfiguration=new ApplicationConfiguration({
//    name:"primzel",
//    secretKeys:[
//        {phrase:phraseP,token:jwt.sign(phraseP,privateP),method:"POST",secret:privateP},
//        {phrase:phrasePU,token:jwt.sign(phrasePU,privatePU),method:"PUT",secret:privatePU},
//        {phrase:phraseD,token:jwt.sign(phraseD,privateD),method:"DELETE",secret:privateD},
//        {phrase:phraseG,token:jwt.sign(phraseG,privateG),method:"GET",secret:privateG},
//        {phrase:phraseO,token:jwt.sign(phraseO,privateO),method:"OPTION",secret:privateO}
//    ]
//});
//
//applicationConfiguration.save();