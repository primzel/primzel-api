var fs = require("fs");
var uuid = require('node-uuid');
var Utils = {};
var path = require("path");

Utils.upload = function (file) {


    if (file == undefined)
        return false;

    var uuidCode = uuid.v1();

    var serverePath = path.join(__dirname, "../public/uploads/" + uuidCode + ".");

    var ext = file.path.split(".");

    ext = ext[ext.length - 1];

    serverePath = serverePath + ext;

    var flag = "/uploads/" + uuidCode + "." + ext;

    fs.readFile(file.path, function (err, data) {

        if (err) {
            flag = null;
            console.log(err);
            return;
        }

        fs.writeFile(serverePath, data, function (err) {
            if (err) {
                flag = null;
                console.log("file write fail  " + err);
            }
        });

    });

    fs.unlinkSync(file.path);

    return flag;

}


module.exports = Utils;