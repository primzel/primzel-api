var Constants = require('./Constants');

function DataCallback(req, res, next, model) {
    this.request = req;
    this.response = res;
    this.next = next;
    this.model = model;

}

DataCallback.prototype.get=function(){

    var req = this.request;
    var resp = this.response;
    var next = this.next;
    model = this.model;

    return function (err, data) {

        if (err || data == undefined || data == null) {
            console.log(err);
            var res = {status: Constants.HTTP_ERROR_QUERY_ON_MODEL, error: "unable to query on " + model};

            resp.send(res);
        }
        else {
            var res = {status: Constants.HTTP_SUCCESS, record: data};

            resp.send(res);
        }

    }
}

DataCallback.prototype.insert = function () {

    var req = this.request;
    var resp = this.response;
    var next = this.next;
    model = this.model;

    return function (err, data) {

        if (err || data == undefined || data == null) {
            console.log(err);
            var res = {status: Constants.HTTP_ERROR_CREATE_ERROR, error: "unable to create " + model};

            resp.send(res);
        }
        else {
            var res = {status: Constants.HTTP_SUCCESS, record: data};

            resp.send(res);
        }

    }
}

module.exports = DataCallback;