var Constants = {};

//Success Code For HTTP Request
Constants.HTTP_SUCCESS = 200;

Constants.HTTP_ERROR_CREATE_ERROR = 1000;

Constants.HTTP_ERROR_QUERY_ON_MODEL = 1001;

Constants.HTTP_ERROR_OBJECT_NOT_FOUNR = 1002;

//To Improve Reafablity Of Code


module.exports = Constants;