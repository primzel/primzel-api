function Page(data, pageSize, pageCount) {
    this.data = data;
    this.pageSize = pageSize;
    this.pageCount = pageCount;
}

Page.prototype.getInstance = function () {

    return {data: this.data, pageSize: this.pageSize, totalPage: this.pageCount};
}

module.exports = Page;