var express = require('express');
var router = express.Router();
var Media = require('../Model/Media');
var Utils = require('../Utils/Utils');
var URL = require("url");
var Constants = require('../Utils/Constants');
var ObjectId = require("mongoose").Types.ObjectId;

//---------------------------------------------------------------------------------------------------------//

/***
 *  Request Handlers
 *
 * **/
var multipart = require("connect-multiparty");
var multipartMiddleWare = multipart();


router.post("/", multipartMiddleWare, function (req, res, next) {

    var url = Utils.upload(req.files.mediaFile);

    var response = {status: Constants.HTTP_SUCCESS, target_url: req.protocol + "://" + req.headers.host + url};

    var media = new Media({"url": response.target_url, "type": req.files.mediaFile.type});

    media.save();

    response.recordId = media._id;

    res.json(response);
});

router.get("/:id", function (req, res, next) {

    Media.findOne({_id: new ObjectId((req.params.id))}, {_id: 1, url: 1, type: 1}, function (err, result) {

        if (err)
            return err;

        res.json(result)
    });

});


module.exports = router;