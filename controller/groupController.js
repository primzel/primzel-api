var express = require('express');
var Router = express.Router();
var Group = require('../Model/Group');
var User = require('../Model/User');
var ObjectId = require("mongoose").Types.ObjectId;
var DataCallbacks = require('../Utils/DataCallbacks');
var Page = require('../Utils/Page');
var Constants = require('../Utils/Constants');
var app = express();
var GroupJoinRequest = require('../Model/GroupJoinRequest');

Router.post("/:userId/group/", function (req, res, next) {

    var group = new Group(req.body);

    group.userId = req.params.userId;

    group.save(new DataCallbacks(req, res, next, "Group").insert());
});

Router.post("/:userId/group/:groupId", function (req, res, next) {

    var userId = req.params.userId;

    var groupId = req.params.groupId;

    Group.findOne({_id: new ObjectId(groupId), isDeleted: false}, {}, function (err, group) {

        if (err || group == null) {
            console.error(err);

            if (group == null) {
                res.send({
                    status: Constants.HTTP_ERROR_OBJECT_NOT_FOUNR,
                    message: "group does not exist with id " + groupId
                });
            }
            return;
        }

        User.findOne({_id: new ObjectId(userId), isDeleted: false}, {}, function (err, user) {

            if (err || user == null) {
                console.error(err);
                if (user == null) {
                    res.send({
                        status: Constants.HTTP_ERROR_OBJECT_NOT_FOUNR,
                        message: "user does not exist with id " + userId
                    });
                }
                return;
            }

            if (group.joinModeration == true) {

                new GroupJoinRequest({userId: userId, groupId: groupId}).save();
            }
            else {

                var groupIds = user.groups === undefined || user.groups === null ? new Array() : user.groups;

                if (groupIds.indexOf(groupId) < 0) {

                    groupIds.push(groupId);
                }

                user.groups = groupId;

                user.save();

            }

            res.send({status: Constants.HTTP_SUCCESS, record: user});
        });

    });

});

Router.get("/:userId/group/", function (req, res, next) {

    var pageSize = parseInt(req.query.pageSize);

    var pageNumber = parseInt(req.query.pageNumber);

    var userId = req.params.userId;

    Group.paginate({userId: userId}, pageNumber, pageSize, function (err, pageCount, paginatedResults) {

        if (err) {

            next(err);
        } else {

            res.send(new Page(paginatedResults, pageSize, pageCount).getInstance());
        }
    });

});


module.exports = Router;