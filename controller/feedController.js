var express = require('express');
var router = express.Router();
var Feed = require('../Model/Feed');
var ObjectId = require("mongoose").Types.ObjectId;
var Page = require('../Utils/Page');
var Constants = require('../Utils/Constants');


router.get("/", function (req, res, next) {

    var pageSize = parseInt(req.query.pageSize);

    var pageNumber = parseInt(req.query.pageNumber);

    Feed.paginate({}, pageNumber, pageSize, function (err, pageCount, paginatedResults) {

        if (err) {

            next(err);
        } else {

            res.send(new Page(paginatedResults, pageSize, pageCount).getInstance());
        }
    });

});

router.post("/", function (req, res, next) {

    var feed = new Feed(req.body);

    var flag = feed.save(function (err, data) {

        if (err || data == undefined || data == null) {
            console.log(err);

            var response = {status: Constants.HTTP_ERROR_CREATE_ERROR, error: "unable to create feed"};

            res.send(response);
        }
        else {

            var response = {status: Constants.HTTP_SUCCESS, record: data};

            res.send(response);
        }
    });
});


module.exports = router;