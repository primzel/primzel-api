var express = require('express');
var router = express.Router();
var User = require('../Model/User');
var Constants = require('../Utils/Constants');
var ObjectId = require("mongoose").Types.ObjectId;


router.post('/', function (req, res, next) {

    var user = new User(req.body);

    user.save();

    var response = {status: Constants.HTTP_SUCCESS, record: user};

    res.send(response);

});


module.exports = router;